varcode	cropcode	variety
Q183	sugarcane	Q183
R109001	Mixtcane	R109001
R109003	Mixtcane	R109003
R109005	Mixtcane	R109005
R109901	Mixtcane	R109901
R109902	Mixtcane	R109902
R109903	Mixtcane	R109903
R109904	Mixtcane	R109904
R109905	Mixtcane	R109905
R109906	Mixtcane	R109906
R109907	Mixtcane	R109907
R109908	Mixtcane	R109908
R109909	Mixtcane	R109909
R570	sugarcane	R570
R572	sugarcane	R572
R573	sugarcane	R573
R575	sugarcane	R575
R577	sugarcane	R577
R579	sugarcane	R579
R585	Mixtcane	R585
R83288	sugarcane	R83288
R932022	sugarcane	R932022
RB72454	sugarcane	RB72454
SM8116	sugarcane	SM8116
SP701284	sugarcane	SP701284
SP733108	sugarcane	SP733108
SP791011	sugarcane	SP791011
SP801842	sugarcane	SP801842
SP832847	sugarcane	SP832847
WI78402	Mixtcane	WI78402
WI79460	Mixtcane	WI79460
WI79461	Mixtcane	WI79461
WI80542	Mixtcane	WI80542
WI81456	Mixtcane	WI81456
WI8615	Mixtcane	WI8615
R586	sugarcane	R586
R109001-sy	Mixtcane	R109001
R109003-sy	Mixtcane	R109003
R109005-sy	Mixtcane	R109005
R109901-sy	Mixtcane	R109901
R109902-sy	Mixtcane	R109902
R109903-sy	Mixtcane	R109903
R109904-sy	Mixtcane	R109904
R109905-sy	Mixtcane	R109905
R109906-sy	Mixtcane	R109906
R109907-sy	Mixtcane	R109907
R109908-sy	Mixtcane	R109908
R109909-sy	Mixtcane	R109909
R570-sy	sugarcane	R570
R585-sy	Mixtcane	R585
WI78402-sy	Mixtcane	WI78402
WI79460-sy	Mixtcane	WI79460
WI79461-sy	Mixtcane	WI79461
WI80542-sy	Mixtcane	WI80542
WI81456-sy	Mixtcane	WI81456
WI8615-sy	Mixtcane	WI8615
B37172	sugarcane	B37172
B80	sugarcane	B80
B82	sugarcane	B82
CO312	sugarcane	CO312
CO6806	sugarcane	CO6806
CP70321	sugarcane	CP70321
CP76331	sugarcane	CP76331
CP881762	sugarcane	CP881762
F152	sugarcane	F152
F160	sugarcane	F160
H564848	sugarcane	H564848
LF669657	sugarcane	LF669657
M37664	sugarcane	M37664
MEX68200	sugarcane	MEX68200
N14	sugarcane	N14
N41	sugarcane	N41
NCO376	sugarcane	NCO376
NIF5	sugarcane	NIF5
Q110	sugarcane	Q110
WI79460-reb	Mixtcane	WI79460
R579-reb	sugarcane	R579
BBZ92076-reb	sugarcane	BBZ92076
WI81456-reb	Mixtcane	WI81456
WI79461-reb	Mixtcane	WI79461
ZN6-ag	sugarcane	ZN6
ZN7-ag	sugarcane	ZN7
Q117-ag	sugarcane	Q117
HOCP96540-ag	sugarcane	HOCP96540
N25-ag	sugarcane	N25
NCO376-ag	sugarcane	NCO376
R570-ag	sugarcane	R570
RB72454-ag	sugarcane	RB72454
CR74250-ag	sugarcane	CR74250
