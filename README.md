MOSICAS, a simple process-based crop model to study the growth of giant C4 grasses
=============================

*Christina Mathias & Jean-François Martiné, UPR AIDA, Cirad*

*Contact: mathias.christina@cirad.fr*

*ChristinaMathias. (2020, April 25). ChristinaMathias/MOSICAS: First realease (Version v1.0.0). Zenodo. http://doi.org/10.5281/zenodo.3766222*

*Actualized January 2023*

Licence : <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International</a>.



___

# Model overview

The Mosicas model is a simple crop model adapted to annual or semi-perenial giant C4 crops. The model simulates the crop growth in biomass influenced by soil water balance and air temperature. According to plant parameters defining if the crop is harvest for the first time or if it’s a regrowth after harvest, the model simulate a daily growth of the leaf area index. Intercepted radiation are then converted to dry mass accumulation through the radiation use efficiency. Carbon is partitioned into various compartments: roots, leaves, stem and eventually sugar reserve. Necessary weather data includes daily temperature, precipitation, global radiations and evapotranspiration. Necessary soil data includes the soil water holding capacity and an estimation of soil depth.

The Mosicas model was developed as an R project (R Core Team, 2020). The code is open-source (GNU GPLv3 license) and archived on Zenodo (https://doi.org/10.5281/zenodo.3766222). Full documentation is available on "MOSICAS_model_description". The project includes R script to run simple simulations, sensitivity analysis and parameter optimization. It also includes R script to create model input from the sugarcane ECOFI database (Christina et al., 2020). The Mosicas model has been originally developed by Martiné and Todoroff (2002) as a fortran source code associated to an excel plateforme and its original code and manual are also open-source in Github repositories. Additionally a web version of the model (named Mosiweb) was developed using an html source code (https://smartis.re/HOME). An overview of the Mosicas model is presented in Fig. 1.


![](Fig/Fig1.jpg)
Fig 1. Overview of the Mosicas model and its different version (Mosicas, MosicasR and Mosiweb). The main step of crop growth within the model and how water (Ws), temperature stress (Ts) or thermal time( °d) influence growth are presented.

___

# Litterature 

+ Martiné, J. F., & Todoroff, P. (2002). Le modèle de croissance Mosicas et sa plateforme de simulation Simulex: état des lieux et perspectives. https://agritrop.cirad.fr/519377/

+ Christina, M., Chaput, M., Martiné, J. F., & Auzoux, S. (2020). ECOFI: a database of sugar and energy cane field trials. Open data Journal for Agricultural Research, 6: 14-18. https://doi.org/10.18174/odjar.v6i0.16322

+ Christina, M., Jones, M. R., Versini, A., Mézino, M., Le Mezo, L., Auzoux, S., ... & Gérardeaux, E. (2021). Impact of climate variability and extreme rainfall events on sugarcane yield gap in a tropical Island. Field Crops Research, 274, 108326. https://doi.org/10.1016/j.fcr.2021.108326

