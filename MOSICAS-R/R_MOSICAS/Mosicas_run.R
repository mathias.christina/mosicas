Mosicas_RUN <- function(Input_read=Input_read,setIni=setIni,setInput=setInput,setOutput=setOutput,
                        output_R=F,output_excel=T,ISENS=0,IOPTI=0,Newpar=NULL){
  
  #### Read simulations run
  Treatment_read <- TREATMENT_READ(FILE='TREATMENT_Mosicas.csv',SEP=';',SetInput=setInput,
                                   Treatcode=Input_read$RUN,Input_read=Input_read)

  #### Open output (list of data.frame)
  MATOUT <- OPEN_OUTPUT(OUTNAME=Input_read$OUTNAME,INIDATE=Treatment_read$begdate,
                        ENDDATE=Treatment_read$enddate, TREATCODE=Input_read$RUN,
                        IFREQ=Input_read$IFREQ,IOBS=Input_read$IOBS)
  
  ### list of running or not runngin simulations
  ISTOP <- rep(0,Input_read$NBRUN)
  
  ##################################################
  #### Beginning of treatment loop
  ##################################################
  
  for (Itreat in 1:Input_read$NBRUN){
    
    #  print('Beginning treatment loop')
    
    ################ Choice of treatment
    Treatment_ID <- TREATMENT_ID(TREATMENT_read=Treatment_read,ITREAT=Itreat)
    
    ############### read weather station information
    Weatherstation_input <- WEATHERSTATION_INPUT (SetInput=setInput,FILE='WEATHER_STATION_Mosicas.csv',SEP=";",
                                                  WSATCODE =Treatment_ID$wstacode)
    
    ############### read weather data
    Weather_data <- WEATHER_DATA(WSATCODE=Treatment_ID$wstacode,begdate=Treatment_ID$begdate,enddate=Treatment_ID$enddate,
                                 SetInput=setInput)
    
    IDNA <- sapply(2:6, function(i) length(which(is.na(Weather_data[,i]))))
    if(sum(IDNA)>0){ISTOP[Itreat]=1}
    
    ############### irrigation data
    Weather_Irrig_data <- IRRIG_DATA(IRRIMODE=Input_read$IRRIGMODE,IRRICODE=Treatment_ID$irricode,
                                     IRRIFREQ=Input_read$IRRIGFREQ,IRRIMM=Input_read$IRRIGMM,
                                     begdate=Treatment_ID$begdate,enddate=Treatment_ID$enddate,SetInput=setInput,
                                     Weatherdata=Weather_data)
    
    #NDAYSIMUL = max(Weather_Irrig_data$julday)
    NDAYSIMUL = as.numeric(as.Date(Treatment_ID$enddate,format='%d/%m/%Y')-as.Date(Treatment_ID$begdate,format='%d/%m/%Y'))+1
    DATESIMUL <- Weather_Irrig_data$date
    
    ############### Read soil generic input
    Soilgen_input <- SOILGEN_INPUT (SetInput=setInput,FILE='SOILGEN_Mosicas.csv',SEP=";",
                                    SOILCODE =Treatment_ID$soilcode)
    
    ############### Read soil layer input
    Soillayer_input <- SOILLAYER_INPUT (SetInput=setInput,FILE='SOILLAYER_Mosicas.csv',SEP=";",
                                        SOILCODE =Treatment_ID$soilcode,
                                        Soilgeninput=Soilgen_input)
    
    ############### Read soil initialization input
    Soilini_input <- SOILINI_INPUT (SetInput=setInput,FILE='SOILINI_Mosicas.csv',SEP=";",
                                    TREATCODE =Treatment_ID$treatcode,Soillayerinput=Soillayer_input,
                                    TreatmentID=Treatment_ID)
    
    ############### Total soil water capacities
    Calcsoilwatercapacities <- CALCSOILWATERCAPACITIES(NLAYER=Soilgen_input$nblayer,SOILLAYER_input=Soillayer_input,
                                                       SOILINI_input=Soilini_input,Soilgen_input=Soilgen_input)
    
    ############### Forcing interception, to do
    isobei=0
    
    ############### plant parameter
    PARAM <- Plant_param_input(SetInput=setInput,SEP=';',VARCODE=Treatment_ID$varcode,CYCLE=Treatment_ID$cycle)
    
    ############### Case sensitivity analysis
    if((ISENS==1)|(IOPTI==1)){
      for(i in 1:dim(Newpar)[2]){
        ID <- which(names(PARAM)==names(as.data.frame(Newpar))[i])
        if(ISENS==1){ival=Itreat}
        if(IOPTI==1){ival=1}
        PARAM[[ID]] <- Newpar[ival,i]
      }
    }
    
    ############## Plant observation ##########
    if((Input_read$IOBS==1)&(Input_read$IFREQ==1)){
      MATOBS <- OBS_READ(FILE='OBS_plant.csv',SEP=';',SetInput=setInput,
                         Treatcode=Treatment_ID$treatcode)
      MATOBSSOIL <- OBS_READ_SOIL(FILE='OBS_soil.csv',SEP=';',SetInput=setInput,
                         Treatcode=Treatment_ID$treatcode)
    }
    
    ###################################################
    ############### Initialisation variable ###########
    # output
    #ISENS=0 # 1= sensitivity analysis
    #WRITE=F # output flag
    
    ## calculated output variables
    spari = 0 ; spr = 0 ; sirr = 0
    setp =0;sswdef =0;sswdfrue =0;sswdflai =0
    srgx =0;setm =0;setr =0;stmp =0
    strp =0;srunoff =0;sevap =0;sdrain =0
    stmo =0;spar=0
    
    ## Plant growth
    
    ## thermal time
    sdd=0
    emergsdd=0;sddlai=0.;
    humaersdd=0
    
    ### blades
    
    ### stalks
    
    nbstem=0.;
    astdurjourp=12.
    hum_stm=0.
    sug_stm=0
    
    ### lai
    lai=0.;ei=0.
    ### stalk height
    htvd=0
    
    ### biomasses
    dmt=0.
    dmstru=0;#mst=0;
    dmbaer=0.;
    dmstm=0;dmsug=0
    dmstst=0;dmtbl=0;dmgbl=0;dmroot=0;
    fmstm=0
    
    ## soil
    swc <- Soilini_input$swc
    swcwp <- Soillayer_input$swcwp
    swcfc <- Soillayer_input$swcfc # dul
    swcsat <- Soillayer_input$swcsat # sat
    swcwp <- Soillayer_input$swcwp
    swcsat_mm <- Calcsoilwatercapacities$swcsat_mm 
    swcfc_mm <- Calcsoilwatercapacities$swcfc_mm 
    swcwp_mm <- Calcsoilwatercapacities$swcwp_mm 
    swcini_mm <- Calcsoilwatercapacities$swcini_mm
    dlayer <- Soillayer_input$thickness
    
    ## Soil evaporation and soil parameters- RItchie 1972
    sumes1=100.*(1-sum((swc-swcwp)/(swcfc-swcwp)*dlayer)/sum(dlayer))#Calcsoilwatercapacities$tsw) 
    sumes2=0.
    swef=0.9-0.00038*(Soillayer_input$thickness[1]-30)**2
    t=0
    U=5.0
    alphaes=3.5
    gam=0.019;
    Ks=4.5;A=82.5
    
    
    # plant transpiration and water stress
    psilb=PARAM$psilb
    swdfrue=1;swdflai=1;swdef=1
    
    ### Vertisol parameters
    Wver=0.3; K2ver=0.6  
    
    ###  divers,général
    
    nage=0 #initialisation days counter
    jat=0; forc1eff=0
    
    ############# Initialisation of soil water balance parameters 
    #if(!is.na(Soilgen_input$soiltype)){
    #  if (Soilgen_input$soiltype=="latosol") {rootgrowth=0.03; roottb=0.5}}
    
    
    ####### root profile initialisation
    nlayer <- max(Soillayer_input$layernum)
    dlayer = Soillayer_input$thickness
    rldmax = PARAM$rldmax
    rwumax = PARAM$rwumax
    roottb = PARAM$roottb # 0° by default
    
    ### limitation of profrac (root depth) down to soil depth
    # total soil depth
    dep <- sum(dlayer[1:nlayer])
    # root front depth, cm
    rootfront=0 
    # root front growth velocity, cm/°d
    rootgrowth = PARAM$rootgrowth #0.08 valeur dans Ceres
    # maximum root front, cm
    rootfrontmax = min(c(dep,Treatment_ID$rootdepth))
    ## initialisation of root length density distribution, cm/cm3
    rlv <- rep(0,nlayer)
    
    ## initialisation rootfront and rlv when ratoons
    if (Treatment_ID$cycle>0) {
      dep <- 0 ; for (i in 2:nlayer){dep <- c(dep,dlayer[i-1]+dep[i-1])}
      rootfront=rootfrontmax
      
      for (i in 1:nlayer){
        if(rootfront>dep[i]){
          if (rootfront >= (dep[i]+dlayer[i])) {
            v1=384649.* (1/(dep[i]+30))^(3.4163)
            v2=384649.* (1/(dep[i]+dlayer[i]+30))^(3.4163)
            rlv[i]=(v1+v2)/2}
          if (rootfront < (dep[i]+dlayer[i])){
            v1=384649.* (1/(dep[i]+30))^(3.4163)
            v2=384649.* (1/(rootfront+30))^(3.4163)
            rlv[i]=(v1+v2)/2}
        }}
    }
    
    ############### End initialisation variables ######
    
    ######################################
    
    ######## begin daily loop ############
    if(ISTOP[Itreat]==0){
      
      for (iday in 1:NDAYSIMUL){
        
        ########################################################
        ######## Daily variable calculation ####################
        
        
        # Minimum temperature
        tempmn <- Weather_Irrig_data$tn[iday]
        # Maximum temperature
        tempmx <- Weather_Irrig_data$tx[iday]
        # Average temperature
        tmo=(tempmn+tempmx)*0.5
        # sum of degree day (base 0)
        stmo = stmo + tmo
        
        # Global radiation
        solrad <- Weather_Irrig_data$rg[iday]
        # Photosynthetic active radiation
        par=0.5*solrad;  
        # sum of incident active radiation
        spar=spar+par
        
        # Julian day for astronomic calculation
        jcal = as.numeric(format(DATESIMUL[iday],"%j"))
        ########################################################

        ## astronomic calculation
        ASTRONO <- Astrono(jcal,lati=Treatment_ID$trialat,longi=Treatment_ID$trialong,alti=Treatment_ID$trialalt,
                           astlatrad,astdr,astdecl,astomeg,astrgo,astrgx,astdurjour,astdurjourp)

        ## water balance
        WATERBALANCE <- Mosiwas_water_balance(Input_read=Input_read,Weather_Irrig_data=Weather_Irrig_data,PARAM=PARAM,
                                              Calcsoilwatercapacities=Calcsoilwatercapacities,
                                              iday=iday,swef=swef,tmo=tmo,psilb=psilb,
                                              swc=swc,swcsat=swcsat,swcfc=swcfc,swcwp=swcwp,
                                              swcwp_mm=swcwp_mm,swcfc_mm=swcfc_mm,swcini_mm=swcini_mm,
                                              dlayer=dlayer,nlayer=nlayer,
                                              Ks=Ks,A=A,lai=lai,sumes1=sumes1,U=U,
                                              sumes2=sumes2,t=t,alphaes=alphaes,gam=gam,
                                              roottb=roottb,rldmax=rldmax,rwumax=rwumax,
                                              rootfront=rootfront,rootgrowth=rootgrowth,rootfrontmax=rootfrontmax,
                                              dep=dep,rlv=rlv,
                                              Wver=Wver,K2ver=K2ver)

        ### Stalks appearance and senescence
        EMERG <- emerg(tempmn=tempmn,tempmx=tempmx,emergsdd=emergsdd,nbstem=nbstem, 
                       emergtb=PARAM$emergtb,emergtt=PARAM$emergtt)

        ### Calculation of Green leaf area index and interception
        LAIGLOB <- laiglob(tempmn=tempmn,tempmx=tempmx,
                           laitb=PARAM$laitb,laigrowth=PARAM$laigrowth,
                           laiwksen=PARAM$laiwksen,laimax=PARAM$laimax,
                           swdflai=WATERBALANCE$swdflai,sddlai=sddlai,lai=lai,nbstem=nbstem)
        
        INTERCEP <-  intercep(isobei=isobei,obei=NULL,ke=PARAM$ke,lai=LAIGLOB$lai,
                              solrad=solrad)
        
        ### Convertion into above ground dry mass (convertion)
        CONVERT <- convert(ruemax=PARAM$ruemax,ruetk=PARAM$ruetk,ruetopt=PARAM$ruetopt,
                           tempmn=tempmn,tempmx=tempmx,pari=INTERCEP$pari,
                           solrad=solrad,astrgx=ASTRONO$astrgx,
                           swdfrue=WATERBALANCE$swdfrue,gdmt=gdmt,dmt=dmt,p01=PARAM$p01,p04=PARAM$p04)
        
        ### partition into aboveground dry mass
        PARTDMT <-  partdmt(Treatment_ID$cycle,gdmt=CONVERT$gdmt,sdd=stmo, dmt=CONVERT$dmt,
                            gdmroot=gdmroot,dmroot=dmroot,gdmbaer=gdmbaer,dmbaer=dmbaer,
                            prootini=PARAM$prootini,prootdec=PARAM$prootdec,prootend=PARAM$prootend)
        
        ### Partition of above ground into stem dry mass
        PARTSTEM <- partstem (pstemini=PARAM$pstemini,pstemend=PARAM$pstemend,pstemdec=PARAM$pstemdec,
                              dmbaer=PARTDMT$dmbaer,gdmbaer=PARTDMT$gdmbaer,
                              jat=jat,gdmstm=gdmstm,dmstm=dmstm)
        
        ### Partition of millable stalks into sugar
        PARTSUGAR <-  partsugar(tempmn, tempmx,gdmstm=PARTSTEM$gdmstm,dmstm=PARTSTEM$dmstm,swdfrue,swdflai,
                                pstrutb=PARAM$pstrutb,pstrutgrowth=PARAM$pstrutgrowt,
                                psugend=PARAM$psugend,pstrudec=PARAM$pstrudec,
                                gdmsug=gdmsug,dmsug=dmsug)
        
        ### Yield and stem water content calculation
        HUMSTEM <- humstem(dmstm=PARTSTEM$dmstm,hum_stm=hum_stm,dmsug=PARTSUGAR$dmsug,
                           fmstm=fmstm,sug_stm=sug_stm,jat=jat,
                           humstemini=PARAM$humstemini,humstemdec=PARAM$humstemdec)

        ### Above ground water content calculation
        HUMCAN <- humcan(humaerini=PARAM$humaerini, humaerend=PARAM$humaerend, humaertb=PARAM$humaertb, 
                         humaerdec=PARAM$humaerdec,
                         hum_aer=hum_aer,tempmx=tempmx,tempmn=tempmn,
                         humaersdd=humaersdd,dmbaer=PARTDMT$dmbaer)
        
        ############## daily variable sum and actualization ######
        
        ######## Sum of variables
        spari <- INTERCEP$pari+ spari
        spr <- Weather_Irrig_data$rr[iday] + spr
        setp <- Weather_Irrig_data$etp[iday] + setp
        sirr <- WATERBALANCE$irrdose + sirr
        sswdef <- sswdef + WATERBALANCE$swdef
        sswdfrue <- WATERBALANCE$swdfrue+ sswdfrue
        sswdflai <- WATERBALANCE$swdflai+ sswdflai
        srgx <- ASTRONO$astrgx +srgx
        setm <- WATERBALANCE$etm+setm
        setr <- WATERBALANCE$etr+setr
        stmp <- WATERBALANCE$tmp+stmp
        strp <- WATERBALANCE$trp+strp
        srunoff <- WATERBALANCE$runoff+srunoff
        sevap <- WATERBALANCE$es+sevap
        sdrain <- WATERBALANCE$deepdrain+sdrain
        
        ######## Re-initialising value for the next day
        emergsdd <- EMERG$emergsdd
        nbstem <- EMERG$nbstem
        sddlai <- LAIGLOB$sddlai
        lai <- LAIGLOB$lai
        ei <- INTERCEP$ei
        pari <- INTERCEP$pari
        dmt <- CONVERT$dmt 
        dmroot <- PARTDMT$dmroot 
        dmbaer <- PARTDMT$dmbaer 
        dmstm <- PARTSTEM$dmstm 
        dmsug <- PARTSUGAR$dmsug 
        fmstm <- HUMSTEM$fmstm
        sug_stm <- HUMSTEM$sug_stm
        hum_stm <- HUMSTEM$hum_stm
        hum_aer <- HUMCAN$hum_aer
        humaersdd <- HUMCAN$humaersdd
        jat <- PARTSTEM$jat
        swc <- WATERBALANCE$swc
        rootfront <- WATERBALANCE$rootfront
        rlv <- WATERBALANCE$rlv
        ###########################################

                ########## Writing daily output
        OUTPUT_ITREAT <- Write_output(Matout=MATOUT[[Itreat]],Input_read=Input_read,Itreat=Itreat,iday=iday,ISENS=ISENS,
                     Treatment_ID=Treatment_ID,Weather_Irrig_data=Weather_Irrig_data,
                     PARTSTEM=PARTSTEM,LAIGLOB=LAIGLOB,INTERCEP=INTERCEP,EMERG=EMERG,HUMSTEM=HUMSTEM,PARTSUGAR=PARTSUGAR,
                     CONVERT=CONVERT,PARTDMT=PARTDMT,HUMCAN=HUMCAN,WATERBALANCE=WATERBALANCE,ASTRONO=ASTRONO,
                     stmo=stmo,spar=spar,srgx=srgx,spari=spari,spr=spr,sirr=sirr,setp=setp,setm=setm,setr=setr,
                     stmp=stmp,strp=strp,sswdef=sswdef,sswdfrue=sswdfrue,sswdflai=sswdflai,sdrain=sdrain,srunoff=srunoff,
                     sevap=sevap,tmo=tmo)

        MATOUT[[Itreat]] <- OUTPUT_ITREAT
        
        #counter(iday)
        
      }
      
      
      if((Input_read$IOBS==1)&(Input_read$IFREQ==1)){
        MATOUT[[Itreat]] <- merge(MATOUT[[Itreat]],MATOBS[,-c(1,3)], by='dates',all.x=T,all.y=F)
        MATOUT[[Itreat]] <- merge(MATOUT[[Itreat]],MATOBSSOIL[,-c(1)], by='dates',all.x=T,all.y=F)
      }
      
      print(Itreat)
    }}
  
  
  MATOUTTEMP <- MATOUT[which(ISTOP==0)]
  MATOUT <- do.call("bind_rows", MATOUTTEMP)
  
  # création tableau excel
  if(output_excel==T){
    
    write.table(x = MATOUT,paste(setIni,'/output/RUN_Mosicas_',Input_read$OUTNAME,'.csv',sep=''),
                row.names = F,col.names = T,quote=F,dec='.',sep=';')
    }
  
  
  if(output_R==T){
    save(MATOUT,file=paste(setIni,'/output/RUN_Mosicas_',Input_read$OUTNAME,'.RData',sep=''))}
  
  return(MATOUT)
  
}
